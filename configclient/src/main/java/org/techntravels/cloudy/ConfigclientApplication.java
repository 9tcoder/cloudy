package org.techntravels.cloudy;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RefreshScope
@RestController
public class ConfigclientApplication {

	@Value("${custom.user.name}")
	public String userName;

	@GetMapping(value = "/custom/user")
	public String getUserName() {
		return userName;
	}
	
	@GetMapping(value = "/custom/employee", produces= {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public Employee getEmployee() {
		Employee employee = new Employee();
		employee.name="Aman";
		employee.ecode="3561";
		employee.age=27;
		Address address = new Address();
		address.country="India";
		address.state="Haryana";
		address.city="Gohana";
		employee.address = address;
		return employee;
	}

	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication(ConfigclientApplication.class);
		springApplication.addListeners(new ApplicationPidFileWriter());
		springApplication.run(args);
	}
}

@XmlRootElement
class Employee{
	String name;
	Address address;
	String ecode;
	Integer age;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getEcode() {
		return ecode;
	}
	public void setEcode(String ecode) {
		this.ecode = ecode;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	
}
class Address{
	String country;
	String state;
	String city;
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
}